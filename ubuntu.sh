echo "running conda installation"
wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh
bash ./Anaconda3-2021.05-Linux-x86_64.sh 
echo "running tmux installation an plugin"
conda install -c conda-forge tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
git clone https://github.com/tmux-plugins/tmux-resurrect ~/.tmux/plugins/tmux-resurrect
git clone https://github.com/tmux-plugins/tmux-continuum ~/.tmux/plugins/tmux-continuum
cp ./.tmux.conf ~/.tmux.conf
tmux source ~/.tmux.conf
echo "running SpaceVim installation an plugin"
curl -sLf https://spacevim.org/install.sh | bash
conda install -c conda-forge nodejs
npm -g install remark
npm -g install remark-cli
npm -g install remark-stringify
npm -g install remark-frontmatter
npm -g install wcwidth
npm install --global prettier
cp ./init.toml ~/.SpaceVim.d/init.toml
conda install -c conda-forge ctags
